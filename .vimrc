" =============================================================================
" = Initialize =
" =============================================================================

" Pre-checks
let s:exec_list = ['wl-paste', 'node', 'deno', 'color-theme']
"let s:exec_list = ['wl-paste', 'color-theme', 'deno']
for s:exec in s:exec_list
  if !executable(s:exec)
    echoerr printf('[vim] `%s` is needed!', s:exec)
    finish
  endif
endfor

syntax on

" Color schemes associated with dark/light OS themes
let g:themes = {
  \ 'dark': 'base16-onedark',
  \ 'light': 'base16-one-light',
\ }

" =============================================================================
" = Functions =
" =============================================================================

function! SetColorVim()
  let currTheme = ( system('color-theme --get') =~ "light" ? "light" : "dark" )
  execute 'colorscheme ' . g:themes[currTheme]
endfunc

function! ToggleColor()
  call system('color-theme --toggle')
  call SetColorVim()
endfunc

" =============================================================================
" = Events (AUtocom Groups ) =
" =============================================================================

"  By default, the indent is 2 spaces.
let g:markdown_recommended_style = 0
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set autoindent
autocmd FileType python setlocal tabstop=4 shiftwidth=4

" =============================================================================
" = Options =
" =============================================================================

" Navigation
set mouse=a " Enable mouse scrolling, cursor clicks

" Line ending
set textwidth=80
set nowrap
set formatoptions-=t
set colorcolumn=81

" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase

" 256 colors for terminal (not sure if this really works)
set termguicolors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

set nomodeline "Disable per-file vim instructions, might not be safe

" =============================================================================
" = Keybindings =
" =============================================================================

" Leader key
let mapleader = ","

" Disable arrow keys in command mode
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" Window movement
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Tab movement
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" Reload vimrc
nnoremap <Leader>sv :source $MYVIMRC<CR>

" Color
nmap <Leader>c :call ToggleColor()<cr>

" Clipboard (needed to work with Wayland)
xnoremap "+y y:call system("wl-copy", @")<cr>
nnoremap "+p :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
nnoremap "*p :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p

" =============================================================================
" = Plugin Pre-Config =
" =============================================================================



" =============================================================================
" = Plugins =
" =============================================================================

" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" UI
Plug 'tinted-theming/base16-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Navigation
Plug 'tpope/vim-vinegar'

" Linting
"Plug 'dense-analysis/ale'

" Autocomplete
Plug 'Shougo/ddc.vim'
Plug 'vim-denops/denops.vim'
Plug 'Shougo/ddc-filter-matcher_head'
Plug 'Shougo/ddc-filter-sorter_rank'
Plug 'Shougo/ddc-source-around'
Plug 'Shougo/ddc-ui-native'
Plug 'Shougo/ddc-ui-pum'
Plug 'Shougo/pum.vim'

" Language Server Protocol
Plug 'shun/ddc-source-vim-lsp'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" =============================================================================
" = Plugin Post-Config =
" ============================================================================= " File browser (netrw)
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

" Status bar (airline)
let g:airline_powerline_fonts = 1

" ddc.vim - config
call ddc#custom#patch_global({
  \ 'ui': 'pum',
  \ 'sources': ['vim-lsp', 'around'],
  \ 'sourceOptions': {
    \ '_': {
      \ 'matchers': ['matcher_head'],
      \ 'sorters': ['sorter_rank'],
    \ },
    \ 'around': {
      \ 'mark': 'A',
    \ },
    \ 'vim-lsp': {
      \ 'mark': 'lsp',
    \ },
  \ },
  \ 'sourceParams': {
    \ 'around': {
      \ 'maxSize': 500,
    \ },
  \ },
\ })

" ddc.vim - keybindings
inoremap <silent><expr> <TAB>
      \ pum#visible() ? '<Cmd>call pum#map#insert_relative(+1)<CR>' :
      \ (col('.') <= 1 <Bar><Bar> getline('.')[col('.') - 2] =~# '\s') ?
      \ '<TAB>' : ddc#map#manual_complete()
inoremap <S-Tab> <Cmd>call pum#map#insert_relative(-1)<CR>
inoremap <C-n>   <Cmd>call pum#map#select_relative(+1)<CR>
inoremap <C-p>   <Cmd>call pum#map#select_relative(-1)<CR>
inoremap <C-y>   <Cmd>call pum#map#confirm()<CR>
inoremap <C-e>   <Cmd>call pum#map#cancel()<CR>

" ddc.vim - configure LSP (Language Server Protocols)
let g:lsp_settings_servers_dir = '~/.vim/lsp'
let g:lsp_settings_filetype_typescript = ['typescript-language-server']
" Open a TypeScript in Vim, then run :LspInstallServer

" ddc.vim - start ddc.vim
au FileType typescript,typescriptreact call ddc#enable()

" =============================================================================
" = Colorscheme =
" =============================================================================

call SetColorVim()

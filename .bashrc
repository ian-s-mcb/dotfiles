#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# Prepare title bar string ("tty: working dir")
# Explained here:
#   https://unix.stackexchange.com/questions/306716/meaning-of-e0-in-ps1-in-bashrc
title='\e]0;tty: \W\a'
# Set prompt and title bar
PS1="[\u@\h \W]\$ \[$(echo -e "$title")\]"

#
# Added by ian-s-mcb
#
eval `dircolors`
[ -r ~/.dircolors ] && eval `dircolors ~/.dircolors`

# complete -cf sudo
source /usr/share/bash-completion/bash_completion

# Vagrant folder
export VAGRANT_HOME='/mnt/storage/envs/vagrant'

# Misc
export GIT_EDITOR=vim
export EDITOR=vim
export PATH="$PATH:$HOME/dev/dev-current/scripts"

# Python
export PIPX_HOME='/mnt/storage/envs/python'
export PIPX_BIN_DIR="$PIPX_HOME/bin"
export PIPX_MAN="$PIPX_HOME/share/man"
export PATH="$PATH:$PIPX_BIN_DIR"

# JS
export NVM_DIR='/mnt/storage/envs/node'
source /usr/share/nvm/init-nvm.sh

# Golang related
export GOPATH='/mnt/storage/envs/go'
export GOCACHE="$GOPATH/cache"
export PATH="$PATH:$GOPATH/bin"

# Rust related
export CARGO_HOME='/mnt/storage/envs/rust/cargo'
export RUSTUP_HOME='/mnt/storage/envs/rust/rustup'
export PATH="$PATH:$CARGO_HOME/bin"

# Deno related
DENO_DIR='/mnt/storage/envs/deno'
DENO_INSTALL_ROOT="$DENO_DIR/bin"
export PATH="$PATH:$DENO_INSTALL_ROOT"

# Required for ssh-agent systemd service
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# K8s alias/autocomplete
source <(kubectl completion bash)
alias k=kubectl
complete -F __start_kubectl k

# Aliases - Misc
alias r='ranger'
alias heroku='/opt/heroku/bin/heroku'
alias nb='jupyter-notebook --no-browser'
alias c19='cd ~/dev/dev-current/c19-homeless && nd && py && workon c19'
alias t3='cd ~/dev/dev-current/wsnr-lobby-server && nd'
alias vi='vim'
alias k='kubectl'
alias rfltr='sudo reflector -c US -f 10 -p https --verbose --save /etc/pacman.d/mirrorlist'
alias bp='cd ~/storage/best-part/music && vim `ls 20* | sort | tail -1`'
alias wsk='wshowkeys -a bottom  -F "serif 50" -t 2 -b 282c3488 -f ff0000'

# Aliases - Hardware
alias pink='speaker-test -t pink -p 1'
alias bat='cat /sys/class/power_supply/*/capacity'
alias mpow='bluetoothctl -- power on && bluetoothctl -- connect 20:19:08:01:1A:78'
alias nob='bluetoothctl -- power on && bluetoothctl -- connect 41:42:2B:09:B1:82'
alias key='bluetoothctl -- power on && bluetoothctl -- connect DC:2C:26:EB:A1:39'
alias bud='bluetoothctl -- power on && bluetoothctl -- connect 24:29:34:BB:9E:17'
alias btoff='bluetoothctl -- power off'
alias boff='light -S 0'
alias bon='light -I'
# Westchester County, NY - run in background
alias wls='wlsunset -l 41.0 -L -73.7 &'

# Firefox
export MOZ_ENABLE_WAYLAND=1

# Fix colors of VM terminal sessions
export TERM=xterm-256color

# Gameboy Advance emulator
export PATH="$PATH:/opt/visualboyadvance-m/build"

# Set window title to name of last command
#   * Sed command accepts input from history command, and then matches
#     whitespace, index prefix, command name, args and replaces them with
#     just command name
#   * Based on the following snippet:
#        https://stackoverflow.com/a/7110386
trap 'echo -ne "\033]2;$(history 1 | sed "s/^\s*[0-9]*\s*\(\S*\).*/\1/g")\007"' DEBUG
